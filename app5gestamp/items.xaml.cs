﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace app5gestamp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class items : ContentPage
    {
        TodoItemManager manager;
        public items()
        {
            InitializeComponent();
            manager = TodoItemManager.DefaultManager;
            

        }

        private async void OnButtonClicked(object sender, EventArgs e)
        {
            tList.ItemsSource = await manager.GetTodoItemsAsync();
        }
    }
}
